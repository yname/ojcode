import turtle as t
t.pensize(2)
t.speed(0)

#刀柄
t.seth(-30)
t.color('black','brown')
t.begin_fill()
t.fd(60)
t.circle(-15,180)#如果逆时针画弧，直接前进就能把刀身和刀柄的直线绘制出来
t.fd(60)
t.end_fill()
#刀身深灰
t.up()
t.goto(0,0)#不需要改变方向，因为刀柄和刀身在同一条直线
t.pd()
t.fillcolor('#c0c0c0')
t.begin_fill()
for i in range(2):
    t.fd(150)
    t.lt(90)
    t.fd(80)
    t.lt(90)
t.end_fill()
#刀身浅白
t.up()
t.right(90)
t.fd(-60)#因为整体是斜着的，所以goto不方便，直接用倒退即可
t.left(90)
t.color('#f5f5f5')
t.begin_fill()
for i in range(2):
    t.fd(150)
    t.lt(90)
    t.fd(20)
    t.lt(90)
t.end_fill()

t.ht()
t.done()