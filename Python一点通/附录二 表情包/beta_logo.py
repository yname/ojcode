import turtle as t
t.speed(0)
#外框
t.color('#E6A92E')
t.begin_fill()
for i in range(4):
    t.fd(200)
    t.left(90)
t.end_fill()
#字母B
t.up()
t.goto(55,40)
t.pd()
t.color('white')
t.begin_fill()
t.fd(70)
t.circle(60/2,180)
t.seth(0)
t.circle(60/2,180)
t.fd(70)
t.goto(55,40)
t.end_fill()
#B小尾巴
t.fd(-10)
t.begin_fill()
t.sety(20)
t.goto(85,40)
t.setx(55)
t.end_fill()
#三角
t.color('#3A9FA1')
t.up()
t.goto(80,80)
t.seth(30)
t.pd()
t.begin_fill()
for i in range(3):
    t.fd(40)
    t.left(120)
t.end_fill()

t.ht()
t.done()