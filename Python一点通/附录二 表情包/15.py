import turtle as t
t.speed(0)
t.pensize(2)
#外圈
t.up()
t.goto(35,-85)
t.pd()
t.color('orange','gold')
t.begin_fill()
for i in range(8):
    t.fd(50)
    t.lt(90)
    t.fd(50)
    t.rt(45)
t.end_fill()
#圆脸
t.up()
t.home()
t.dot(150,'yellow')
#右眼
t.goto(30,10)
t.dot(15,'brown')
#左眼
t.goto(-30,10)
t.dot(15,'brown')
#嘴巴
t.pensize(3)
t.pencolor('brown')
t.goto(-15,-20)
t.pd()
t.seth(-30)
t.circle(30,60)


t.ht()
t.done()