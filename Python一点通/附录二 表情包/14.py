import turtle as t
t.pensize(2)
t.speed(0)
#主框架
t.color('brown','#b22222')
t.begin_fill()
for i in range(2):
    t.fd(100)
    t.rt(90)
    t.fd(150)
    t.rt(90)
t.end_fill()
#上半部分
t.color('red')
t.begin_fill()
t.fd(100)
t.rt(90)
t.fd(20)
t.circle(-50,180)
t.end_fill()
#黄色圆圈
t.up()
t.goto(50,-66)
t.dot(30,'yellow')
t.pencolor('brown')
t.goto(52,-78)
t.write('￥',align='center',font=(20))

t.ht()
t.done()