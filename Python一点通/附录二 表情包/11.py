'''
因为涉及到覆盖问题，所以第一遍画的时候顺序是主体、红色、引线、黄色
当绘制好以后，再改变程序的顺序为黄色、引线、红色、主体
'''
import turtle as t
t.speed(0)
#黄色部分
t.up()
t.goto(220,100)
t.pd()
t.color('yellow')
t.begin_fill()
for i in range(6):
    t.circle(10,180)
    t.right(180-360/6)
t.end_fill()
#引线
t.up()
t.goto(130,120)
t.pencolor('black')
t.pensize(10)
t.pd()
t.circle(-50,30)
t.circle(60,30)
#红色部分
t.up()
t.goto(80,125)
t.pd()
t.seth(-45)
t.pensize(3)
t.color('red')
t.begin_fill()
t.fd(60)
t.lt(90)
t.fd(30)
t.lt(90)
t.fd(60)
t.lt(90)
t.fd(30)
t.end_fill()
#炸弹主体
t.up()
t.home()
t.dot(300,'black')




t.hideturtle()
t.done()