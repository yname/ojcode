import turtle as t
t.up()
t.speed(0)
t.goto(-100,-100)
t.pensize(3)
#方块底色
t.color('blue')
t.pd()
t.begin_fill()
for i in range(4):
    t.fd(200)
    t.lt(90)
t.end_fill()
#右侧虚线
t.up()
t.goto(85,-85)
t.pensize(5)
t.pencolor('white')
t.seth(90)
for i in range(6):
    t.pd()
    t.fd(20)
    t.up()
    t.fd(10)
#左侧虚线
t.up()
t.goto(-85,-85)
t.pensize(5)
t.pencolor('white')
t.seth(90)
for i in range(6):
    t.pd()
    t.fd(20)
    t.up()
    t.fd(10)
#中间竖线
t.up()
t.goto(0,-70)
t.pensize(15)
t.seth(90)
t.pd()
t.fd(120)
#三角形
t.up()
t.pensize(3)
t.setx(-20)
t.seth(0)
t.pd()
t.color('#ffffff')
t.begin_fill()
for i in range(3):
    t.fd(40)
    t.lt(120)
t.end_fill()


t.ht()
t.done()