import turtle as t
t.speed(0)
#三角
t.pensize(10)
t.color('black','yellow')
t.begin_fill()
for i in range(3):
    t.fd(200)
    t.lt(120)
t.end_fill()
#黑色三角
t.up()
t.rt(120)
t.fd(-100)
t.lt(60)#通过等边三角形的角平分线推算出转60度正好垂直
t.pd()
t.color('black')
t.pensize(2)
t.begin_fill()
t.goto(200,0)
t.home()
t.end_fill()
#黄色轮廓
#如果有难度，该轮廓可以不画
t.pencolor('yellow')
t.up()
t.goto(10,9)#轮廓粗细为10，估算出起笔点要让出轮廓的份额
t.pd()
for i in range(3):
    t.fd(180)#去掉两边的轮廓粗细
    t.lt(120)
#长方形
t.up()
t.seth(90+60)#通过对顶角相等，准确求出朝向方向
t.goto(125,30)#通过计算边长，可以得到准确的x，Y坐标估算即可
t.pd()
t.color('yellow')
t.begin_fill()
for i in range(2):
    t.fd(50)
    t.lt(90)
    t.fd(8)
    t.lt(90)
t.end_fill()
#小三角
t.up()
t.seth(60)
t.goto(80,40)
t.pd()
t.begin_fill()
for i in range(3):
    t.fd(20)
    t.lt(120)
t.end_fill()


t.ht()
t.done()