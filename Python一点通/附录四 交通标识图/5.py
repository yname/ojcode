import turtle as t
t.speed(0)
#三角
t.pensize(10)
t.color('black','yellow')
t.begin_fill()
for i in range(3):
    t.fd(200)
    t.lt(120)
t.end_fill()
#慢
t.up()
t.goto(100,20)
t.write('慢',align='center',font=('微软雅黑',50,'bold'))

t.ht()
t.done()