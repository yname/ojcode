import turtle as t
t.speed(0)
#三角
t.pensize(10)
t.color('black','yellow')
t.begin_fill()
for i in range(3):
    t.fd(200)
    t.lt(120)
t.end_fill()
'''
#感叹号(偷懒方法)
t.up()
t.goto(125,0)#因为用的中文感叹号，所以坐标要有一定的偏移量
t.write('！',align='center',font=('宋体',100))
'''
#感叹号
t.up()
t.goto(100,30)
t.dot(20,'black')
t.sety(50)
t.seth(90)
t.pd()
s=1
for i in range(30):
    t.pensize(s)
    t.fd(2)
    s+=1



t.hideturtle()
t.done()