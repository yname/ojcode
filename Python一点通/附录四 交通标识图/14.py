import aifc
import turtle as t
t.pensize(3)
#蓝底色
t.color('RoyalBlue')
t.begin_fill()
for i in range(4):
    t.fd(200)
    t.lt(90)
t.end_fill()
#白框
t.up()
t.pencolor('white')
t.goto(30,15)
t.pd()
for i in range(4):
    t.fd(140)
    t.circle(15,90)
#P
t.up()
t.goto(110,5)
t.write('P',align='center',font=('黑体',140))


t.ht()
t.done()