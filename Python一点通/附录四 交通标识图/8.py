import turtle as t
#底圈
t.pensize(15)
t.pencolor('red')
t.up()
t.goto(0,-100)
t.pd()
t.circle(100)
#红色斜线
t.up()
t.pensize(15)
t.pencolor('#ff0000')
t.goto(0,-100)
t.seth(0)
t.circle(100,40)
t.seth(90+40)
t.pd()
t.fd(200)
#左转弯
t.up()
t.pensize(20)
t.pencolor('black')
t.goto(30,-60)
t.pd()
t.seth(90)
t.fd(80)
t.circle(5,90)
t.fd(40)
#左转箭头(可以看作两个平行四边形拼接的)
t.up()
t.pensize(3)
t.color('#000000')
t.goto(-40,25)#通过前面的转弯坐标可以推算出来这个坐标
t.pd()
t.seth(0)
t.begin_fill()
for i in range(2):#上方平行四边形
    t.fd(20)
    t.lt(40)
    t.fd(35)
    t.lt(140)
t.end_fill()
t.begin_fill()
for i in range(2):
    t.fd(20)
    t.rt(40)
    t.fd(35)
    t.rt(140)
t.end_fill()



t.ht()
t.done()