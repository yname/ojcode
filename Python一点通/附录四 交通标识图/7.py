import turtle as t
t.up()
t.dot(200,'red')
t.color('white')
t.pensize(3)
t.goto(-70,-15)
t.pd()
t.begin_fill()
for i in range(2):
    t.fd(140)
    t.lt(90)
    t.fd(30)
    t.lt(90)
t.end_fill()


t.ht()
t.done()