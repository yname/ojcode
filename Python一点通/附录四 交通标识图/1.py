import turtle as t
#三角
t.pensize(10)
t.fillcolor('yellow')
t.begin_fill()
for i in range(3):
    t.forward(200)
    t.left(120)
t.end_fill()
#竖框
t.pensize(3)
t.up()
t.goto(90,15)
t.pd()
t.color('black')
t.begin_fill()
for i in range(2):
    t.fd(10)
    t.lt(90)
    t.fd(100)
    t.lt(90)
t.end_fill()
#横框
t.goto(90,60)
t.begin_fill()
for i in range(2):
    t.fd(50)
    t.lt(90)
    t.fd(10)
    t.lt(90)
t.end_fill()

t.ht()
t.done()