from ssl import ALERT_DESCRIPTION_ILLEGAL_PARAMETER
import turtle as t
t.speed(0)
t.pensize(3)
#长方部分
t.color('RoyalBlue')
t.begin_fill()
for i in range(2):
    t.fd(180)
    t.lt(90)
    t.fd(60)
    t.lt(90)
t.end_fill()
#尖尖部分
t.begin_fill()
t.goto(-30,30)
t.goto(0,60)
t.end_fill()
#白框
t.color('#ffffff')
t.up()
t.goto(5,5)
t.pd()
t.begin_fill()
for i in range(4):
    t.fd(50)
    t.lt(90)
t.end_fill()
#红十字
t.up()
t.goto(30,-10)
t.pencolor('#ff0000')
t.write('+',align='center',font=('微软雅黑',50,'bold'))
#急救站
t.goto(120,10)
t.pencolor('white')
t.write('急 救 站',align='center',font=('微软雅黑',20,'bold'))


t.ht()
t.done()