import turtle as t
t.dot(200,'blue')
t.up()
t.pensize(3)
t.color('white')
t.goto(-60,50)
t.seth(-90)
t.pd()
t.begin_fill()
for i in range(5):
    t.fd(45)
    t.lt(120)
t.end_fill()
t.seth(0)
#弯曲的线
t.pensize(12)
t.fd(-10)
t.fd(50)
t.circle(-30,180)
t.fd(50)
t.circle(-15,180)
t.pensize(10)
t.fd(100)
#吹气口
t.pensize(8)
t.seth(90)
t.fd(15)
t.fd(-30)


t.ht()
t.done()