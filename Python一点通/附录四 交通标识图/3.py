import turtle as t
t.speed(0)
#三角
t.pensize(10)
t.color('black','yellow')
t.begin_fill()
for i in range(3):
    t.fd(200)
    t.lt(120)
t.end_fill()
#红绿灯的黑底色
t.pensize(30)
t.up()
t.goto(100,35)
t.pd()
t.lt(90)
t.fd(60)  #画笔本身粗细30，所以前进60+原本30=90，正好放下3个红绿灯
#红绿灯
t.up()
t.dot(28,'red')#要有留白，所以不能为30
t.fd(-30)
t.dot(28,'yellow')
t.fd(-30)
t.dot(28,'green')



t.ht()
t.done()