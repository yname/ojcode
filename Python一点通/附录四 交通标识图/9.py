import turtle as t
t.up()
t.speed(0)
t.pensize(20)
t.sety(-100)
t.pd()
t.pencolor('red')
#圆和斜线
t.circle(100,360+40)#多走一点，确定斜线起笔位置
t.seth(90+40)#确定斜线出笔方向
t.fd(200)
#竖线
t.up()
t.goto(0,-60)
t.pensize(20)
t.pencolor('black')
t.pd()
t.seth(90)
t.fd(90)
#三角
t.pensize(3)
t.up()
t.setx(-25)
t.seth(0)
t.pd()
t.color('#000000')
t.begin_fill()
for i in range(3):
    t.fd(50)
    t.lt(120)
t.end_fill()


t.ht()
t.done()