import turtle as t
t.speed(0)
t.up()
t.sety(-100)
t.pensize(3)
t.color('blue')
t.pd()
#底圆
t.begin_fill()
t.circle(100)
t.end_fill()
#白色半圆
t.color('#0000ff','#ffffff')
t.circle(100,-45)#先倒回去一点，表示起笔
t.begin_fill()
t.circle(100,90)
t.end_fill()
#数字
t.up()
t.goto(0,-60)
t.pencolor('white')
t.write('50',align='center',font=('楷体',100,'bold'))

t.ht()
t.done()