import turtle as t
t.speed(0)
#三角
t.pensize(10)
t.color('black','yellow')
t.begin_fill()
for i in range(3):
    t.fd(200)
    t.lt(120)
t.end_fill()
#长方形
t.pensize(3)
t.up()
t.goto(50,30)
t.pd()
t.color('black')
t.begin_fill()
for i in range(2):
    t.fd(100)
    t.lt(90)
    t.fd(20)
    t.lt(90)
t.end_fill()
#拱形凸起
t.up()
t.goto(75,50)
t.dot(30,'black')
t.setx(75+50)
t.dot(30,'black')


t.done()