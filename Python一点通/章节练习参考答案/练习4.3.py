#本题要注意各个部分的大小比例关系
import turtle as t
t.shape('turtle')
t.pensize(5)
#大圆
t.circle(150)
#嘴巴
t.penup()
t.goto(40,70)
t.pendown()
t.circle(50)
#左眼
t.penup()
t.goto(-70,220)
t.pendown()
t.forward(50)
#右眼
t.penup()
t.goto(70,220)
t.pendown()
t.forward(50)

t.hideturtle()