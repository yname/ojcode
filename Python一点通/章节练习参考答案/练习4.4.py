'''
本题要求掌握同心圆的坐标计算方法
讲解时建议不要用课本的尺寸讲，防止照抄
先画大圆和画小圆是一样的
'''
import turtle as t
t.shape('turtle')
t.pensize(3)
#大圆
t.circle(100)
#小圆
t.penup()
t.goto(0,40)#一定要讲清楚这个坐标是怎么算出来的
t.pendown()
t.circle(60)