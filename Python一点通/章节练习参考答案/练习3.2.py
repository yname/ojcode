'''
通过绘制正5边形引出外角的计算
'''
import turtle as t
t.shape('turtle')
t.pensize(3)

t.forward(160)
t.left(360/5)
t.forward(160)
t.left(360/5)
t.forward(160)
t.left(360/5)
t.forward(160)
t.left(360/5)
t.forward(160)
t.left(360/5)
