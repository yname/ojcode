'''
此题是练习4.4的提高，重点还是掌握坐标的计算
讲解时不要用课本的尺寸
先画内框或外框都可以
'''
import turtle as t
t.shape('turtle')
t.pensize(3)
t.pencolor('blue')
#外框
t.forward(300)
t.left(90)
t.forward(300)
t.left(90)
t.forward(300)
t.left(90)
t.forward(300)
t.left(90)
#内框起笔点
t.penup()
t.goto(70,70)#重点！要知道怎么算出来的
t.pendown()
#内框
t.forward(160)
t.left(90)
t.forward(160)
t.left(90)
t.forward(160)
t.left(90)
t.forward(160)
t.left(90)