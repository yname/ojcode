'''
先画正方形或者先画三角形都可以
等边三角形的边长与正方形边长相等
'''
import turtle as t
t.shape('turtle')
#正方形
t.forward(200)
t.right(90)
t.forward(200)
t.right(90)
t.forward(200)
t.right(90)
t.forward(200)
t.right(90)
#三角形
t.forward(200)
t.left(120)
t.forward(200)
t.left(120)
t.forward(200)
t.left(120)