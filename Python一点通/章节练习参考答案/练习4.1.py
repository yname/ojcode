import turtle as t
t.shape('turtle')
t.pensize(3)
#画笔粗细、形状等虽然没有要求必须写，但建议让学生写上，作为巩固
t.forward(200)
t.left(90)
t.forward(200)
t.goto(0,0)