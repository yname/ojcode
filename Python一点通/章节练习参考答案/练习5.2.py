# 五角星的线条色和填充色一致即可
import turtle as t
t.shape('turtle')
t.pencolor('#ff0000')#用十六进制和red都一样
t.fillcolor('#ff0000')
t.begin_fill()
#五角星绘制过程
t.forward(200)
t.right(144)#旋转度数要记住
t.forward(200)
t.right(144)
t.forward(200)
t.right(144)
t.forward(200)
t.right(144)
t.forward(200)
t.right(144)

t.end_fill()
#隐藏画笔
t.hideturtle()