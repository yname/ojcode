'''
本题主要考察write方法的使用
'''
import turtle as t
t.up()
t.write('春晓',align='center',font=('黑体',30,'bold'))
t.sety(-50)#用goto也可以，间距自行调整
t.write('春眠不觉晓',align='center',font=('仿宋',20,'normal'))
t.sety(-100)
t.pencolor('blue')
t.write('处处闻啼鸟',align='left',font=('楷体',20,'italic'))