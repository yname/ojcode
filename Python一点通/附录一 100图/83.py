'''
这种图形要从中心点沿某一条边找合适的基础图形
这个图是6个六边形,类似图形还有8个八边形，10个十边形
'''
import turtle as t
for i in range(6):
    for j in range(6):
        t.fd(50)
        t.right(360/6)
    t.right(360/6)

t.done()