import turtle as t
t.speed(0)
def sanjiao(a):
    for j in range(3):
        t.fd(a)
        t.left(120)
#右侧一组三角形
a=40
t.right(30)
for i in range(4):
    sanjiao(a)
    a+=10
#左侧一组三角形
a=40
t.seth(150)#左侧三角形出笔方向
for i in range(4):
    sanjiao(a)
    a+=10
#下方三角形
t.right(90)
sanjiao(-40)
#右上角
t.fd(60)
t.right(180-60)
t.fd(10)
#左上角
t.up()
t.home()
t.seth(90+30)
t.pd()
t.fd(60)
t.left(180-60)
t.fd(10)

t.hideturtle()
t.done()