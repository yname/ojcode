'''
6个长方形组成，所以直接普通循环就可以
'''
import turtle as t
for i in range(6):
    for j in range(2):
        t.fd(100)
        t.right(90)
        t.fd(30)
        t.right(90)
    t.right(360/6)

t.done()