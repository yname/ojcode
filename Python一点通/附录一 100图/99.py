import turtle as t
def one():
    t.circle(-20,100)
    t.circle(20,100)
    t.right(160)
    t.circle(-20,100)
    t.circle(20,100)

for i in range(9):
    one()
    t.right(360/9)

t.ht()
t.done()