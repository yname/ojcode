import turtle as t
t.up()
t.goto(-90,0)#为了画在正中间
t.down()
#雨伞大圆弧
t.seth(90)
t.circle(-90,180)
#小圆弧
for i in range(3):
    t.seth(90)
    t.circle(30,180)
#伞杆
t.up()
t.goto(0,30)
t.pd()
t.seth(-90)
t.fd(100)
#伞柄
t.circle(-10,180)

t.done()