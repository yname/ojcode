'''
6个菱形，直接跑循环就可以
'''
import turtle as t 
for i in range(6):
    for j in range(2):
        t.fd(100)
        t.left(60)
        t.fd(100)
        t.left(120)
    t.left(360/6)

t.done()