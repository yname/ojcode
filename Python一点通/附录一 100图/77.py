'''
思路一：6个三角形构成
思路二：画六边形，再单独画三角形
思路三：画两个三角形（位置不好确定，不建议）
'''
import turtle as t
for i in range(6):
    for j in range(3):
        t.fd(50)
        t.right(120)
    t.fd(50)
    t.left(360/6)

t.done()