import turtle as t
t.speed(10)
t.rt(60)
def one():
    for j in range(3):
        #一个基础菱形
        for i in range(2):
            t.fd(60)
            t.lt(120)
            t.fd(60)
            t.lt(60)
        #多走两条边，最后能回到共同中心点
        t.fd(60)
        t.lt(60)
        t.fd(60)
        t.lt(60)
for i in range(3):
    one()
    t.lt(360/3)
