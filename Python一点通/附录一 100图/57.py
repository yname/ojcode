'''
利用嵌套绘制正方形
'''
import turtle as t
for i in range(4):#一共4组
    for j in range(2):#中间正方形的两条边
        t.fd(50)
        t.rt(90)
    t.left(180)
    for k in range(4):#外侧正方形
        t.fd(50)
        t.rt(90)
    t.right(180)
    for x in range(2):#中间正方形的另外两条边
        t.fd(50)
        t.rt(90)
    t.left(90)#下一组图形的出笔方向

t.done()