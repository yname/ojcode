import turtle as t
def lx1():
    for i in range(2):
        t.fd(100)
        t.right(120)
        t.fd(100)
        t.right(60)
def lx2():
    for i in range(2):
        t.fd(100)
        t.left(60)
        t.fd(100)
        t.left(120)
#下边俩菱形
lx1()
t.fd(100)
lx2()
t.fd(100)
lx1()
#上边俩菱形
t.left(60)
t.fd(100)
t.right(60)
lx2()
t.fd(-200)
lx2()

t.done()