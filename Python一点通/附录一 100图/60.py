'''
可以由两个半圆构成，最后画斜边
也可以先画直角三角形，再画半圆
'''
import turtle as t
#短直角边半圆
t.left(90)
t.circle(50,180)
t.home()
#长直角边半圆
t.right(180)
t.circle(-70,180)
t.home()
#斜边
t.goto(-100,0)
t.goto(0,140)

t.done()