'''
绘制6组枝叶就可以
'''
import turtle as t
def one():#一组枝叶
    for i in range(2):#前两个叶片
        t.fd(30)
        t.left(30)
        t.fd(10)
        t.fd(-10)
        t.rt(60)
        t.fd(10)
        t.fd(-10)
        t.lt(30)
    t.fd(30)
    t.rt(30)
    for i in range(2):#菱形
        t.fd(10)
        t.lt(60)
        t.fd(10)
        t.lt(120)
    t.lt(30)#回正方向


t.seth(90)#确定出笔方向
for i in range(6):
    one()
    t.up()
    t.goto(0,0)
    t.pd()
    t.left(360/6)


t.done()