import turtle as t
#一个叶片
def a():
    t.circle(20,90)
    t.left(90)
    t.circle(20,90)
    t.left(90)
#一组叶片
def one():
    t.seth(90)
    t.fd(20)
    a()
    t.seth(0)
    a()
#三组叶片
for i in range(3):
    one()
#最上方叶片
t.seth(90)
t.fd(20)
t.right(45)
a()

t.done()