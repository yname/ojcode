import turtle as t
def star(n):
    for i in range(n):
        t.fd(100)
        '''
        每个角添加辅助线可以得到不同的三角形，每个三角形都是等腰三角形
        通过观察辅助线，可以得到一个内接正多边形，推测出一个外角为360/n
        '''
        t.right(360/n*2)
        t.fd(100)
        t.left(360/n)
t.left(90)
star(6)
t.done()