import turtle as t
#一片叶子
def a():
    t.circle(10,90)
    t.left(90)
    t.circle(10,90)
    t.left(90)
#一组叶子
def one():
    a()
    t.right(90)
    a()
    t.left(90)
#循环n次
for i in range(5):
    t.circle(-50,360/5)
    one()

t.done()