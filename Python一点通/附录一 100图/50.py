'''
需要计算菱形的角度
调整初始朝向方向
'''
import turtle as t
t.left(30)#改变初始方向
for i in range(4):
    for j in range(2):
        t.fd(100)
        t.right(60)
        t.fd(100)
        t.right(120)
    t.right(360/4)

t.done()