'''
提供一个参考规律：画完一个角，再画角平分线
'''
import turtle as t
t.speed(1)
for i in range(5):
    t.fd(100)
    t.right(144)
    t.fd(100)
    t.right(54)
    t.fd(100*1.9)#没啥好办法，试一下吧(我猜测可能有啥规律或者公式)
    t.fd(-100*1.9)
    t.left(126)#通过五角星内角的度数推算出角平分线旋转角度



t.done()