import turtle as t
def a():#一个叶片
    t.circle(-10,90)
    t.right(90)
    t.circle(-10,90)
    t.right(90)
def one():#一组叶片
    a()
    t.left(90)
    a()
    t.right(90)

t.seth(90)#起笔方向
#前半部分
for i in range(3):
    t.circle(-50,150/3)
    one()
#后半部分
for i in range(3):
    t.circle(50,150/3)
    one()


t.done()