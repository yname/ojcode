'''
经过规律查找发现，长方形直接旋转得不到图形
'''
import turtle as t
for i in range(4):
    for j in range(2):
        t.fd(100)
        t.right(90)
        t.fd(30)
        t.right(90)
    t.left(90)#保证下次起笔的方向

t.done()