import turtle as t

t.up()
t.ht()
name = []
phone = []
# 本程序只是实现了最基本的功能，并没有做深层次的逻辑补充，比如电话号码不是纯数字怎么办等等
def info():
    t.sety(180)
    t.write("电话本选项", align="center", font=("宋体", 18, "bold"))
    t.sety(-60)
    text = "1.添加新联系人\n\n2.修改原有联系人\n\n3.查找联系人电话\n\n4.显示所有联系人电话\n\n5.退出"
    t.write(text, align="center", font=("宋体", 18))


def add():
    name_add = t.textinput("添加联系人", "请输入姓名：")
    if name_add in name:
        print("该联系人已存在，请重新输入")
    else:
        name.append(name_add)
        phone_add = t.textinput("添加联系人", "请输入电话：")
        if len(phone_add) != 11:
            print("电话号码不符合规范，请输入正确的11位电话号码")
        else:
            phone.append(phone_add)
            print("添加联系人成功！")


def change():
    name_change = t.textinput("修改联系人", "请输入姓名：")
    if name_change in name:
        i = name.index(name_change)
        phone[i] = t.textinput("修改联系人", "请输入新的电话号码：")
        print("联系人信息修改成功！")
    else:
        print("该联系人不存在！")


def find():
    name_find = t.textinput("查找联系人", "请输入姓名：")
    if name_find in name:
        i = name.index(name_find)
        print("%s的电话号码是：%s" % (name_find, phone[i]))
    else:
        print("该联系人不存在！")


def outall():
    ans = len(name)
    if ans == 0:
        print("空电话本")
        return
    for i in range(ans):
        print(name[i] + ":" + phone[i])


# 程序主体
info()
while 1:
    cnt = int(t.textinput("电话本系统", "请输入选项："))
    if cnt == 1:
        add()
    elif cnt == 2:
        change()
    elif cnt == 3:
        find()
    elif cnt == 4:
        outall()
    elif cnt == 5:
        print("退出系统")
        exit()
        break
    else:
        print("输入错误，请重新输入！")

t.done()
