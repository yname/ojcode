import turtle as t
import random as r
t.shape('turtle')
t.setup(600,400)#设置窗体大小
#询问玩家信息
name=t.textinput('用户信息','请输入玩家名称:')
age=t.textinput('用户信息','请输入玩家年龄：')
#提示框左下角起笔位置
t.speed(0)
t.penup()
t.goto(-200,-50)
t.pendown()
#绘制方框
for i in range(2):
    t.forward(400)
    t.left(90)
    t.forward(200)
    t.left(90)
#绘制提示文字
t.penup()
t.goto(0,100)
t.write('游戏说明',align='center',font=('黑体',20,'bold'))
t.goto(0,-20)
text='电脑每次投掷三枚骰子\n总点数≥10为大，否则为小\n猜对前进，猜错后退，到达终点结束\n%s岁的%s请开始你的灿烂人生吧'%(age,name)
t.write(text,align='center',font=('黑体',15))
#绘制跑道
t.goto(-250,-120)
t.pensize(3)
t.pendown()
t.forward(500)
t.dot(30,'red')
t.penup()
t.setx(-250)
t.dot(30,'red')
#询问开始
star=t.textinput('龟在囧途','是否开始游戏 Y/N')
if star=='Y':
    f=0
    while 1:
        s1=r.randint(1,6)
        s2=r.randint(1,6)
        s3=r.randint(1,6)
        s=s1+s2+s3
        ans=t.textinput('龟在囧途','请输入点数大小 big OR small ：')
        if s>=10 and ans=='big' or s<10 and ans=='small':
            #猜对向前走，幅度较大
            q=r.randint(50,70)
            print('骰子数为:',s1,s2,s3)
            print('你猜对啦！前进 %d 步'%(q))
            t.forward(q)
            f+=q
        else:
            #猜错向后走，幅度较小
            h=r.randint(10,30)
            print('骰子数为:',s1,s2,s3)
            print('你猜错啦！后退 %d 步'%(h))
            t.forward(-h)
            f-=h
        if f>=495:#控制结束
            break


t.done()